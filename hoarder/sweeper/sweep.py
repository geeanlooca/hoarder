from abc import ABC, abstractmethod
import argparse
import copy
import multiprocessing
import os
from pathlib import Path
import subprocess
from itertools import product

import numpy as np
from box import Box

from hoarder.hoarder import Hoard

from hoarder.sweeper.runners import ParallelRunner, BladeRunner


def generate_config_grid(config):
    sweep_config = copy.deepcopy(config.sweep)
    del config.sweep

    # Preprocess all the sweep parameters to compute the parameter ranges
    for k, v in sweep_config.items():
        if isinstance(v, Box):
            if hasattr(v, "start") and hasattr(v, "end"):
                start = v.start
                end = v.end
                step = 1

                p = []

                if hasattr(v, "num"):
                    p = list(np.linspace(start, end, v.num))

                else:
                    if hasattr(v, "step"):
                        step = v.step
                    else:
                        step = 1

                    while start <= end:
                        p.append(start)
                        start += step

                sweep_config[k] = p

    config_filenames = []
    # Generate all the possible configurations
    for num, x in enumerate(product(*sweep_config.values())):
        comb = Box(zip(sweep_config, x))
        config_copy = copy.deepcopy(config)

        for k, v in comb.items():
            config_copy[k] = v

        # Save the configurations to file

        Path("./configs").mkdir(parents=True, exist_ok=True)
        filename = os.path.join("configs", f"config.{num}.yml")
        config_filenames.append(filename)
        config_copy.to_yaml(filename=filename, default_flow_style=None)

    return config_filenames


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("program")
    parser.add_argument("-c", "--config")

    args, left = parser.parse_known_args()
    config = Hoard.load_params(args.config)
    configs = generate_config_grid(config)

    # Local run
    runner = BladeRunner(args.program, configs, "dev")
    runner.run()
