from abc import ABC, abstractmethod
import tqdm
import argparse
import copy
import multiprocessing
import os
import subprocess
from itertools import product

import numpy as np
from box import Box

from hoarder.hoarder import Hoard


class Runner(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def run(self):
        pass


class ParallelRunner(Runner):
    def __init__(self, program, configs, cores=4):
        self.program = program
        self.configs = configs
        self.pool = multiprocessing.Pool(cores)

    @staticmethod
    def worker(args):
        program = args[0]
        config_file = args[1]
        num = args[2]
        subprocess.Popen(
            f"python {program} --hoard {config_file} --hoard-path {num}",
        ).wait()

    def run(self):
        process_args = [(self.program, c, num) for num, c in enumerate(self.configs)]

        with tqdm.tqdm(total=len(process_args)) as pbar:
            for _ in self.pool.imap_unordered(ParallelRunner.worker, process_args):
                pbar.update(1)


class ClusterRunner(Runner, ABC):
    @abstractmethod
    def submit(self):
        pass


class BladeRunner(ClusterRunner):
    def __init__(self, program, configs, conda_env=None):
        self.program = program
        self.configs = configs
        self.conda_env = conda_env

    def submit(self):
        pass

    def run(self):
        with tqdm.tqdm(total=len(self.configs)) as pbar:
            for num, config_file in enumerate(self.configs):
                job_filename = f"sweep.{num}.job"
                with open(job_filename, "w") as f:

                    f.write("#!/bin/bash")
                    f.write("$ -cwd")
                    f.write("$ -N {name}")
                    f.write("$ -e output")
                    f.write("$ -o output")
                    f.write("$ -m n")
                    f.write("$ -pe parallel {nodes}")

                    if self.conda_env is not None:
                        f.write("conda activate {self.conda_env}")

                    f.write("python {self.program}")

                subprocess.Popen(f"qsub {job_filename}").wait()

                pbar.update(1)


class SLURMRunner(ClusterRunner):
    pass
