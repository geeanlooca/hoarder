# -*- coding: utf-8 -*-
"""Created on Fri Jan 24 10:08:55 2020.

@author: Gianluca
"""

import argparse
import atexit
import datetime
import os
import re
import shutil
import time

import json_tricks
import yaml
from box import Box

import h5py

"""Initialize a safe YAML loader supporting exponential notation."""
loader = yaml.SafeLoader
loader.add_implicit_resolver(
    "tag:yaml.org,2002:float",
    re.compile(
        """^(?:
    [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
    |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
    |\\.[0-9_]+(?:[eE][-+][0-9]+)?
    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
    |[-+]?\\.(?:inf|Inf|INF)
    |\\.(?:nan|NaN|NAN))$""",
        re.X,
    ),
    list("-+0123456789."),
)


class Hoard:
    """Hoard experiment results and data.

    Parameters
    ----------
    path: str
        The directory in which Hoard stores the runs.

    Raises
    ------
    OSError
        If Hoard cannot create the required directories in the filesystem.
    """

    def __init__(self, path):
        parser = argparse.ArgumentParser()
        parser.add_argument("--hoard-path")
        args, left = parser.parse_known_args()

        self.base_path = path

        if not os.path.exists(self.base_path):
            try:
                os.mkdir(self.base_path)
            except OSError:
                print(f"Could not create directory: {self.base_path}")
            else:
                print(f"Successfully created directory: {self.base_path}")

        # Determine the run path from current time
        timestamp = "{:%Y%m%d-%H%M%S.%f}".format(datetime.datetime.now())

        if args.hoard_path is None:
            run_dirname = f"hoard_{timestamp}"
        else:
            run_dirname = f"hoard_{args.hoard_path}_{timestamp}"

        self.run_dir = os.path.join(self.base_path, run_dirname)

        try:
            os.mkdir(self.run_dir)
        except OSError:
            print("Could not create directory for the current run.")

        self.start_time = time.time()
        self.metadata = Box()

        atexit.register(self.finalize)

    def finalize(self):
        """Compute execution time and save metadata to file."""
        self.end_time = time.time()
        self.metadata.execution_time = self.end_time - self.start_time

        self.write_metadata()

    def write_metadata(self):
        """Save the metadata field to json file."""
        self.metadata.to_json(filename=os.path.join(self.run_dir, "metadata.json"))

    def params(self, box, format="yaml"):
        """Store the Box configuration object."""
        accepted_formats = {"json", "yaml"}

        format_ = format.lower()

        if format_ not in accepted_formats:
            raise ValueError("Data format not supported.")

        data_dumper = None

        if format_ == "yaml":
            data_dumper = Box.to_yaml
        elif format_ == "json":
            data_dumper = Box.to_json
        elif format_ == "toml":
            data_dumper = Box.to_toml

        data_dumper(box, filename=os.path.join(self.run_dir, f"config.{format_}"))

    def data(self, data):
        """Store data dictionary in JSON format."""
        if not isinstance(data, dict):
            data = dict(data)

        filename = os.path.join(self.run_dir, "data.json")

        with open(filename, "w") as f:
            json_tricks.dump(data, f, indent=4)

    def array(self, key=None, value=None):
        """Store an array as a dataset in a HDF5 file.

        Examples
        --------
        >>>h = Hoard("runs")
        >>>h.array("data", np.random.random((10,1)))
        >>>h.array("metrics", np.random.random((10,1)))


        You can also store multiple arrays at the same time by passing a dictionary.

        The dictionary:
            data = {

                "params": {
                    "a": 1,
                    "b": 2,
                },

                "x": np.random.random((10,1))
            }

        will create the following groups/datasets:

            h5file["params/a"] = 1
            h5file["params/b"] = 2
            h5file["x"] = np.random.random((10,1))

        """
        datafile_name = os.path.join(self.run_dir, "data.h5")

        if key is None and value is None:
            raise ValueError("Wrong arguments.")

        if key is not None and value is None:
            # Dictionary passed as input

            if isinstance(key, dict):
                # Build a list of (key, value) pairs to insert in a HDF5 file
                nodes = [(k, v) for k, v in key.items()]
                processed = []

                # Loop until the list is empty
                while nodes:
                    parent = nodes.pop(0)
                    current_key = parent[0]

                    if isinstance(parent[1], dict):
                        # Need to go deeper
                        nodes.extend(
                            [(f"{current_key}/{k}", v) for k, v in parent[1].items()]
                        )
                    else:
                        # Leaf node
                        processed.append((current_key, parent[1]))

                with h5py.File(datafile_name, "a") as f:
                    for k, v in processed:
                        f[k] = v
            else:
                raise ValueError("Not a dictionary.")
        else:
            with h5py.File(datafile_name, "a") as f:
                f[key] = value

    def file(self, file):
        """Save a file to the run directory."""
        basename = os.path.basename(file)
        dest_name = os.path.join(self.run_dir, basename)
        shutil.move(file, dest_name)

    @staticmethod
    def load_params(filename=None):
        """Load the content of the YAML file in a Box object."""

        parser = argparse.ArgumentParser()
        parser.add_argument("--hoard")
        args, left = parser.parse_known_args()

        if args.hoard is None and filename is None:
            raise ValueError("Parameter file not specified.")

        if args.hoard is not None:
            filename = args.hoard

        with open(filename, "r") as f:
            content = yaml.load(f, Loader=loader)

            box_config = Box(content, box_dots=True)

            return box_config
