import os
import pathlib
import random
import shutil
import sys

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtGui import QColor, QKeySequence

import h5py

from h5py import Group, Dataset


from PyQt5.QtWidgets import (
    QFileDialog,
    QListWidgetItem,
    QShortcut,
    QTreeWidgetItem,
    QVBoxLayout,
    QMessageBox,
)

from hoarder.gui.main_window import Ui_MainWindow


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, *args, obj=None, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.setupUi(self)

        self.actionOpen_folder.triggered.connect(self.open_directory_slot)
        self.listWidget.currentItemChanged.connect(self.show_config)
        self.listWidget.itemDoubleClicked.connect(self.item_double_click)

        self.save_shortcut = QShortcut(QKeySequence("Ctrl+S"), self)
        self.save_shortcut.activated.connect(self.save_notes)

        self.opendir_shortcut = QShortcut(QKeySequence("Ctrl+O"), self)
        self.opendir_shortcut.activated.connect(self.open_directory_slot)

        self.refresh_shortcut = QShortcut(QKeySequence("Ctrl+R"), self)
        self.refresh_shortcut.activated.connect(self.refresh_slot)

        self.delete_run_shortcut = QShortcut(QKeySequence("Shift+Del"), self)
        self.delete_run_shortcut.activated.connect(self.detele_shortcut_slot)

        self.actionClear_empty_runs.triggered.connect(self.delete_empty_runs)

        self.pushButtonPlot.clicked.connect(self.plot_button_slot)
        self.pushButtonClearPlot.clicked.connect(self.plot.clear)

        # self.actionOpen_folder.triggered.connect(self.plot)

        # a figure instance to plot on
        self._basedir = None

    def plot_button_slot(self):
        """Plot the selected data."""
        item = self.listWidget.currentItem()

        if item is None:
            return

        datafile = os.path.join(self.get_run_directory(item.text()), "data.h5")

        data_x_key = self.comboBoxX.currentText()
        data_y_key = self.comboBoxY.currentText()

        if os.path.exists(datafile):
            with h5py.File(datafile, "r") as f:
                dataX = f[data_x_key]
                dataY = f[data_y_key]
                plt.plot(dataX, dataY)
                self.plot.refresh_canvas()

    def detele_shortcut_slot(self):
        """Delete selected run when pressing Shift+Delete."""
        item = self.listWidget.currentItem()

        if item is None:
            return

        run_dir = self.get_run_directory(item.text())

        try:
            shutil.rmtree(run_dir)
        except FileNotFoundError:
            pass

        self.listWidget.takeItem(self.listWidget.row(item))

    def item_double_click(self, item):
        """Handle the double click action on a run."""
        path = self.get_run_directory(item.text())
        print(path)
        os.startfile(path)

    def refresh_slot(self):
        """Refresh the run list."""
        self.listWidget.clear()
        self.populate_runlist(self._basedir)

    def delete_empty_runs(self):
        """Delete the empty run directories and refresh the run list."""
        for x in range(self.listWidget.count()):

            run = self.listWidget.item(x).text()

            if self.is_run_empty(run):
                run_dir = self.get_run_directory(run)
                shutil.rmtree(run_dir)

        self.listWidget.clear()
        self.populate_runlist(self._basedir)

    def open_directory_slot(self):
        """Open a FileDialog to open an experiment directory."""
        file = QFileDialog.getExistingDirectory(self, "Select Directory")

        if file != "":
            self._basedir = pathlib.Path(file)
            self.populate_runlist(file)

    def is_run_empty(self, run):
        """Check if a given run directory is empty."""
        run_dir = self.get_run_directory(run)
        run_content = os.listdir(run_dir)

        return not run_content

    def lack_config(self, run):
        """Return True if the given run has no config.yaml file."""
        run_dir = self.get_run_directory(run)

        config = os.path.join(run_dir, "config.yaml")

        return not os.path.exists(config)

    def populate_runlist(self, file):
        """Given a directory path, populate the list of runs inside that directory."""
        if file is None:
            return

        for run in os.listdir(file):

            item = QListWidgetItem(run)

            if self.is_run_empty(run):
                item.setBackground(QColor("#FA6A6A"))
            elif self.lack_config(run):
                item.setBackground(QColor("#DFF68F"))

            item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
            item.setCheckState(QtCore.Qt.Unchecked)

            self.listWidget.addItem(item)

    def get_run_directory(self, item):
        """Obtain the absolute path to the provided run."""
        return os.path.join(self._basedir, pathlib.Path(item))

    def read_run_file(self, item, file):
        """Read the content of the specified file from the specified run."""
        run_dir = self.get_run_directory(item)
        fullfile = os.path.join(run_dir, file)

        with open(fullfile, "r") as f:
            content = f.read()

        return content

    def show_config(self, item, prev):
        """Load config and notes file to the respective widgets."""
        if item is None:
            return

        # Read the config file
        try:
            config = self.read_run_file(item.text(), "config.yaml")
            self.textEdit.setText(config)
        except FileNotFoundError as err:
            self.textEdit.setText("")

        # Read the notes
        try:
            notes = self.read_run_file(item.text(), "notes.txt")
            self.textEdit_notes.setText(notes)
        except FileNotFoundError as err:
            self.textEdit_notes.setText("")

        datafile = os.path.join(self.get_run_directory(item.text()), "data.h5")

        combos = (self.comboBoxX, self.comboBoxY)

        current_text = [combo.currentText() for combo in combos]

        [combo.clear() for combo in combos]

        datasets = []

        if os.path.exists(datafile):
            for k in self._parse_hdf5(datafile):
                [combo.addItem(k) for combo in combos]

        for combo, text in zip(combos, current_text):
            if text:
                index = combo.findText(text)
                if index >= 0:
                    combo.setCurrentIndex(index)

        self.populate_filelist(item.text())

    def _parse_hdf5(self, filename):
        """Return a list of dataset paths in `file`.

        """
        with h5py.File(filename, "r") as f:
            # Build a list of (key, value) pairs to insert in a HDF5 file
            # The key is build by concatenating the keys
            nodes = [(k, v) for k, v in f.items()]
            processed = []

            # Loop until the list is empty
            while nodes:
                parent = nodes.pop(0)
                current_key = parent[0]

                if isinstance(parent[1], Group):
                    # Need to go deeper
                    nodes.extend(
                        [(f"{current_key}/{k}", v) for k, v in parent[1].items()]
                    )
                else:
                    # Leaf node
                    processed.append(current_key)

            return processed

    def populate_filelist(self, item):
        """Given a directory, populate the list containing the runs."""
        files = os.listdir(self.get_run_directory(item))
        self.listFiles.clear()

        for file in files:
            self.listFiles.addItem(file)

    def save_notes(self):
        """Handle the saving of the notes to file."""
        item = self.listWidget.currentItem()

        if item is None:
            # No items in the list view
            return

        notes_file = os.path.join(self.get_run_directory(item.text()), "notes.txt")

        notes = self.textEdit_notes.toPlainText()

        if notes != "":
            # Overwrite
            try:
                with open(notes_file, "w") as f:
                    f.write(notes)

                    self.statusbar.showMessage("Notes saved.", 2000)
            except:

                self.statusbar.showMessage("Error saving notes.", 2000)
                pass

        elif notes == "" and os.path.exists(notes_file):
            # Delete the notes file
            os.remove(notes_file)
            self.populate_filelist(item.text())


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()


if __name__ == "__main__":
    main()
