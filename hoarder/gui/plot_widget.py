import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from PyQt5 import QtWidgets


class PlotWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(PlotWidget, self).__init__(parent)

        self._figure = True
        self._canvas = True
        self._toolbar = True
        self._initialized = False

        self.setupUi()

    def setupUi(self):
        if self._initialized:
            return

        self._initialized = True
        layout = QtWidgets.QVBoxLayout(self)
        toolbar = self._toolbar

        self._figure = plt.figure()
        self._canvas = FigureCanvasQTAgg(self._figure)
        self._toolbar = NavigationToolbar2QT(self._canvas, self)
        layout.addWidget(self._toolbar, 0)
        layout.addWidget(self._canvas, 1)

    def refresh_canvas(self):
        self._canvas.draw()

    def clear(self, *args, **kwargs):
        plt.cla()
        self.refresh_canvas()
